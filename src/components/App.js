import React from 'react'
import { View, Text, ScrollView } from 'react-native'
import {Header, Story,Halaman,Halaman1,Pooter,H2,H3,H4,H5,H6} from './index'

const App = () => {
    return (
        <View>
           <Header />
           <ScrollView>
               <Story />
               <Halaman />
               <Halaman1/>
               <H2 />
               <H3 />
               <H4 />
               <H5 />
               <H6 />
           </ScrollView>
           <Pooter/>
        </View>
    )
}

export default App
